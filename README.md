# README #

## What is this repository for? ##

* A simple API for task purposes.
* Demonstrate ability to use Python in daily work without knowing it deeply.

## How do I get set up? ##

### For local development/testing ENV ###

* Run local env by:

    ```commandline
    docker compose build
    docker compose up
    ```
 
* Server is accessible under http://localhost:8000/
* In case of issues with docker and errors like "max depth exceeded" please clean system

    ```commandline
    docker system prune -a
    ```    

* I Found some issue with ${PWD} - checking. temp solution: context definition in [docker-compose.yml] 
* CURL queries should have JSON object type data  
* For mac users Docker require to map project folder to have necessary rights

### For production ENV ### 

* TODO

### ATTENTION!!! ### 

* For this task purposes [./config/sampleapi.ini] file are part of this repo. They should not be included
in to official repository. They should be passed through CI/CD tool like Jenkins, TeamCity, etc.
* Database is instanced in docker container for task purposes. Normaly DB should be outside docker containers. 

## What to add/improve ###

* integration tests
* unit tests for UserService
* code syntax checking with PyChecker, Pylint, or other (some example
  overview https://luminousmen.com/post/python-static-analysis-tools)
* api file structure optimization
* make nicer autodiscovery for model schema creation
* better handle Exceptions for User operations
* consider a bit different approach to handling output from SQlite (SQLAlchemy) in response
* verification for docker build process (testing)

## Used sources/tools/libraries ###

Main packages/tools used:

* PyCharm Community Edition https://www.jetbrains.com/pycharm
* Python https://docs.python.org/3.9/
* PIP https://pypi.org/project/pip/
* Poetry for Python https://python-poetry.org/
* FastAPI library https://fastapi.tiangolo.com
* SQLAlchemy https://www.sqlalchemy.org
* Docker DOC https://docs.docker.com
* Docker compose DOC https://docs.docker.com/compose/
* Markdown language syntax https://www.markdownguide.org/extended-syntax

There was a plan to also introduce separate package for services but I had some problems with package access. Package
was installed ok, but functionality was not accesible. I will try to figure it out.

* Oracle-Devops-Services repo https://bitbucket.org/mirekratman/oracle-devops-services/src/master/

Knowledge grabbed from:

* StackOverflow community answers https://stackoverflow.com/search (for some issues)
* Some Youtube videos that describe some tech specs of used language/libraries
* and the most important - BRAIN ;)

### App requirements ###

For system requirements please look in to:

    https://bitbucket.org/mirekratman/oracle-devops-api-sampleapi/src/master/pyproject.toml

## The task ##

### Description ###

Create a simple http server that provides two services. Everything should be wrapped into a docker container.

* Service 1:
    * Should accept GET requests to the root path (/) with 3 parameters: 'a', 'b' and 'op'. 'a' and 'b' are numbers
      and 'op' is one of the following characters: "-", "/", "*" or "%2b" (uri encoded "+").
    * Server should return JSON response with status, result of the specified operation and current date and
      time.Example:

      ```commandline
      curl "http://localhost:8000/?a=340.234&b=450000000&op=*"
      ```
      ```json
      {
        "status": "ok",
        "date": "2021-04-22 16:31",
        "result": 153105300000.0
      }
      ```
    * Server should be able to handle wrong input, absence of parameters in query and respond with correct HTTP codes,
      for example:

      ```commandline
      curl -v "http://localhost:8000/?b=450000000&op=*"
      ```
      ```json
      {
        "status": "error",
        "reason": "a not found in query string"
      }
      ```
        * example (informative) curl querying process
          ```buildoutcfg
          * Trying 127.0.0.1:8000...
          * TCP_NODELAY set
          * Connected to localhost (127.0.0.1) port 8000 (#0)
          > GET /?b=450000000&op=* HTTP/1.1
          > Host: localhost:8000
          > User-Agent: curl/7.68.0
          > Accept: */*
          * Mark bundle as not supporting multiuse
          * HTTP 1.0, assume close after body
          < HTTP/1.0 400 Bad Request
          < Server: SimpleHTTP/0.6 Python/3.8.5
          < Date: Thu, 22 Apr 2021 14:36:10 GMT
          < Content-type: application/json
          ```

* Service 2:
    * Server should have an sqlite3 database with a table that contains the following fields:
        ```buildoutcfg
        id: integer, autoincrement, mandatory,
        name: text, mandatory,
        address: text, length 100,
        salary: float,
        age: integer, mandatory
        ```
    * and serve one endpoint at path /users/ with two methods: GET and POST.

    * **POST**: Post method should accept json encoded data, check it's validity, ensure all mandatory parameters are
      provided, and save them to the database. Additionally server should verify that name is unique, otherwise (the
      same name has been already in the database) it should return an error.
        * Once a record created in the database the server should throw a response containing all the fields from the
          record. Happy path example:

          ```commandline
          curl -X POST http://127.0.0.1:8000/users/ -d '{ "name": "John", "address": "Praha, 15000", "age": 18, "salary": 0 }'
          ```
          ```json
          {
            "status": "success",
            "result": {
              "id": 1,
              "name": "John",
              "age": 18,
              "salary": 0.0,
              "address": "Praha, 15000"
            },
            "date": "2021-04-23 12:02:56"
          }
          ```

        * Example for the case when provided name is not unique:

          ```json
          {
            "status": "error",
            "reason": "User with such name is already present"
          }
          ```

    * **GET**: Get method should accept "id" and/or "name". Server should check database and return json encoded object
      or 404 if relevant record was not found.
        * Happy path example:
          ```commandline
          curl http://127.0.0.1:8000/users/?id=1
          ```
          ```json
          {
            "status": "success",
            "result": {
              "id": 1,
              "name": "John",
              "age": 18,
              "salary": 0.0,
              "address": "Praha, 15000"
            },
            "date": "2021-04-23 12:17:24"
          }
          ```

        * Example for the case when record is not found:
            ```commandline
            curl "http://127.0.0.1:8000/users/?id=1&name=Smith"
            ```
            ```json
            {
              "status": "error",
              "reason": "Requested user not found"
            }
            ```

        * Example for invalid query:
            ```commandline
            curl "http://127.0.0.1:8000/users/"
            ```
            ```json
            {
              "status": "error",
              "reason": "id or name is required"
            }
            ```


