from typing import Any
from sqlalchemy.orm import Session
from src.Model.User.UserModel import UserModel
from sqlalchemy import and_
from sqlalchemy.exc import InvalidRequestError, IntegrityError
from src.Service.User.UserServiceException import UserServiceExist

class UserService:
    """
    User service for manipulate user data
    """

    def __init__(self, engine):
        self.engine = engine

    def get(self, id: int, name: str) -> UserModel:
        try:
            with Session(self.engine) as session:
                # Set id to -1 for situation when ID or NAME is not passed
                filters = [(UserModel.id == -1)]
                # Set right filters
                if id:
                    filters = [(UserModel.id == id)]
                if name:
                    filters = [(UserModel.name == name)]
                if id and name:
                    filters = [and_(UserModel.id == id, UserModel.name == name)]

                return session.query(UserModel).filter(*filters).first()
        finally:
            session.close()

    def add(self, UserModel: UserModel) -> UserModel:
        try:
            with Session(self.engine) as session:
                session.add(UserModel)
                session.commit()
                session.refresh(UserModel)
                session.close()
                return UserModel
        except IntegrityError as e:
            raise UserServiceExist


