# Custom exception class for calculations
# This could be extended with logging functionality, etc

class CalculationValueError(Exception):
    def __init__(self):
        super().__init__('ValueError: "%s"' % repr(Exception))


class CalculationKeyError(Exception):
    def __init__(self):
        super().__init__('KeyError: "%s"' % repr(Exception))
