import uvicorn
import json
import sqlalchemy
from fastapi import FastAPI, Request, status
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from src.Api.Helper.ResponseHelper import ResponseHelper
from src.Api.Helper.ResultHelper import ResultHelper
from localconfig import LocalConfig

# TODO Temporary solution - usage of oracledevopsservices package needs to be fixed
from src.Service.Validation.CalculationException import CalculationValueError, CalculationKeyError
from src.Service.User.UserServiceException import UserServiceExist
from src.Service.Validation.Calculation import calculateValues
from src.Service.User.UserService import UserService
from src.Model.User.UserModel import UserModel
from src.Model.User.UserModel import UserModelApiSchema

# Services setup
config = LocalConfig('/dist/config/sampleapi.ini')
app = FastAPI()
dbEngine = sqlalchemy.create_engine(
    'sqlite:///{0}.config'.format(config.sqlalchemy.db),
    convert_unicode=True
)
userService = UserService(dbEngine)


# Start server
def start():
    uvicorn.run(
        "src.Api.SampleApi:app",
        host=config.uvicorn.host,
        port=config.uvicorn.port,
        reload=True
    )


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exception: RequestValidationError) -> JSONResponse:
    """
    Handler for RequestValidationError
    """
    responseHelper = ResponseHelper()
    errCount = 0;
    for error in exception.errors():
        # Handler for params issues in query string
        if error['loc'][0] == 'query':
            wrongUrlParam = error['loc'][1]  # TODO - find maybe better way to grab info about param name
            responseHelper.addErrorStatus(
                '({0}: Parameter [{1}] not found in query string)'.format(errCount, wrongUrlParam))

        # Handler for body data issues in query
        if error['loc'][0] == 'body':
            wrongUrlParam = error['loc'][1]  # TODO - find maybe better way to grab info about param name
            responseHelper.addErrorStatus(
                '({0}: Parameter [{1}] is required)'.format(errCount, wrongUrlParam))
        errCount += 1
    responseHelper.addErrorStatus('[Exception message: {0}]'.format(str(exception)))

    return JSONResponse(
        status_code=responseHelper.getStatusCode(),
        content=responseHelper.toJSON()
    )


@app.get("/")
async def index(a: float, b: float, op: str) -> JSONResponse:
    """
    Handler for calculation of values
    """
    responseHelper = ResponseHelper()
    try:
        result = calculateValues(a, b, op)
        responseHelper.setResult(result)
        return JSONResponse(
            status_code=responseHelper.getStatusCode(),
            content=responseHelper.toJSON()
        )
    except CalculationKeyError as e:
        responseHelper.setErrorStatus('wrong parameter in query string')
        return JSONResponse(
            status_code=responseHelper.getStatusCode(),
            content=responseHelper.toJSON()
        )
        pass

    except CalculationValueError as e:
        responseHelper.setErrorStatus('parameter not found in query string')
        return JSONResponse(
            status_code=responseHelper.getStatusCode(),
            content=responseHelper.toJSON()
        )
        pass


@app.get("/users/")
async def getUser(id: int = "", name: str = "") -> JSONResponse:
    """
    Handler for getting user by ID
    """
    responseHelper = ResponseHelper()
    if id or name:
        resultHelper = ResultHelper(
            userService.get(id, name)
        )
        if resultHelper.toJSON() != None:
            responseHelper.setResult(resultHelper.toJSON())
        else:
            responseHelper.setErrorStatus("Requested user not found")
    else:
        responseHelper.setErrorStatus("id or name is required")
    return JSONResponse(
        status_code=responseHelper.getStatusCode(),
        content=responseHelper.toJSON()
    )


@app.post("/users/")
async def addUser(body: UserModelApiSchema) -> JSONResponse:
    """
    Handler for adding new user
    """
    responseHelper = ResponseHelper()
    try:
        userModel = UserModel(
            body.name,
            body.age,
            body.salary,
            body.address
        )

        resultHelper = ResultHelper(
            userService.add(userModel)
        )
        responseHelper.setResult(resultHelper.toJSON())

        return JSONResponse(
            status_code=responseHelper.getStatusCode(),
            content=responseHelper.toJSON()
        )

    except UserServiceExist as e:
        responseHelper.setErrorStatus('User with such name is already present')
        return JSONResponse(
            status_code=responseHelper.getStatusCode(),
            content=responseHelper.toJSON()
        )
        pass
