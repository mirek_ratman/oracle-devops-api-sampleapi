from typing import Any
from sqlalchemy import inspect

class ResultHelper:
    """
    Helper will convert result to JSON
    """
    def __init__(self, result: Any):
        self.result = result

    def toJSON(self):
        if self.result:
            return {c.key: getattr(self.result, c.key)
                for c in inspect(self.result).mapper.column_attrs}
        else:
            return None