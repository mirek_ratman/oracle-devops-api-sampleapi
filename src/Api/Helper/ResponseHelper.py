import json
from typing import Any
from datetime import datetime

# Consts
RESPONSE_NODATA = {"code":201, "text":"no-data"}
RESPONSE_OK = {"code":200, "text":"ok"}
RESPONSE_ERROR = {"code":400, "text":"error"}

class ResponseHelper:
    def __init__(self):
        now = datetime.now()

        self.status = RESPONSE_NODATA["text"]
        self.date = now.strftime("%Y-%m-%d %H:%M")
        self.result = None
        self.reason = ""

    def setResult(self, result: Any, status: str = ""):
        if result:
            self.status = status if status else RESPONSE_OK["text"]
        self.result = result
        # Personaly I will keep "self.reason" field in API response for consistency
        delattr(self, 'reason')

    def setStatus(self, status: str = ""):
        self.status = status

    def getStatusCode(self):
        if self.status == RESPONSE_NODATA["text"]:
            return RESPONSE_NODATA["code"]
        if self.status == RESPONSE_OK["text"]:
            return RESPONSE_OK["code"]
        if self.status == RESPONSE_ERROR["text"]:
            return RESPONSE_ERROR["code"]

    def setErrorStatus(self, reason: str = ""):
        self.status = RESPONSE_ERROR["text"]
        self.reason = reason

    def addErrorStatus(self, reason: str = ""):
        self.status = RESPONSE_ERROR["text"]
        self.reason = self.reason + (", " if self.reason else "") + reason

    def toJSON(self):
        return json.JSONDecoder().decode(json.dumps(self.__dict__))
