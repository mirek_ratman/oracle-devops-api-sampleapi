import json
from typing import Optional
from pydantic import BaseModel
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, BigInteger, SmallInteger, Float, String

Model = declarative_base()


class UserModel(Model):
    """
    User model
    SQLAlchemy model
    """
    __tablename__ = 'User'
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True, unique=True)
    name = Column(String(200), nullable=False, unique=True)
    age = Column(SmallInteger, nullable=False)
    salary = Column(Float(10))
    address = Column(String(100))

    def __init__(self, name, age, salary, address):
        self.name = name
        self.age = age
        self.salary = salary
        self.address = address

    @classmethod
    def initModel(self, dbEngine):
        Model.metadata.create_all(dbEngine)

    @classmethod
    def toJSON(self) -> object:
        return json.JSONDecoder().decode(json.dumps(self.__dict__))


class UserModelApiSchema(BaseModel):
    """
    User model schema for API
    Pydantic model
    """
    name: str
    age: int
    salary: Optional[float] = None
    address: Optional[str] = None

    class Config:
        orm_mode = True
        # schema for API DOC https://pydantic-docs.helpmanual.io/usage/schema/#schema-customization
        schema_extra = {
            "example": {
                "name": "Foo",
                "age": 22,
                "salary": 3500.40,
                "address": "Foo Address 123",
            }
        }
