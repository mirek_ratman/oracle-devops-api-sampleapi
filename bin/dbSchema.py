import sqlalchemy
from sqlalchemy import create_engine
from localconfig import LocalConfig
from src.Model.User.UserModel import UserModel

def init():
    config = LocalConfig("./config/sampleapi.ini")
    dbEngine = sqlalchemy.create_engine(
        'sqlite:///{0}.config'.format(config.sqlalchemy.db),
        convert_unicode=True,
        echo=True
    )
    # List all necessary models
    UserModel.initModel(dbEngine)
