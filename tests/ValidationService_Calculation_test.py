# content of test_expectation.py
import pytest
from typing import Any
from src.Service.Validation.CalculationException import CalculationValueError, CalculationKeyError
from src.Service.Validation.Calculation import calculateValues

# Set mocked data
__mockData: Any = {
    "test_CalculateValue": [
        (["340.234", "450000000", "*"], 153105300000.0),
        (["340.234", "1", "+"], 341.234),
        (["341.234", "1", "-"], 340.234),
        (["100", "10", "/"], 10),
        (["100.2", "10", "%2b"], 110.2)
    ],
    "test_CalculateValueException": [
        (["340.234", "450000000", ""]),
        (["340.234", "450000000", ""]),
        (["340.234", "", "+"]),
        (["", "450000000", "+"]),
        (["", "", "+"]),
        (["", "", ""]),
        # (["10", "10", "+"]) # For temp verification of proper calculation
    ]
}


@pytest.mark.parametrize("passedData, expected", __mockData["test_CalculateValue"])
def test_CalculateValue(passedData, expected):
    """
    Testing calculation
    :param passedData:
    :param expected:
    """
    result = calculateValues(passedData[0], passedData[1], passedData[2])
    assert result == expected


@pytest.mark.parametrize("passedData", __mockData["test_CalculateValueException"])
def test_CalculateValueException(passedData):
    """
    Testing calculation with exceptions
    :param passedData:
    :param expected:
    """
    try:
        result = calculateValues(passedData[0], passedData[1], passedData[2])
        assert False
    except CalculationValueError as e:
        assert True
    except CalculationKeyError as e:
        assert True
